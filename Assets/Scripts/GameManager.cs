using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class GameManager : Singleton<GameManager>
{
    [SerializeField] VerticalLayoutGroup _recordsGroup;
    [SerializeField] RectTransform _recordsRect;
    [SerializeField] Text _timer;
    [SerializeField] Text _totalMonsterCText;
    [SerializeField] Text _notification;
    [SerializeField] Transform _gameOverPanel;
    [SerializeField] Records _records;
    bool _gameInProgress;
    Action _recordNotification;
    float _difficultyStepTime = 20f;
    int _totalMonsterC;
    /// <summary>
    /// _gameTime.
    /// </summary>
    float _gT;
    /// <summary>
    /// _monsterDyingC.
    /// </summary>
    int _mDC;
    /// <summary>
    /// _abilityApplicationC.
    /// </summary>
    int _aAC;
    int _recordScores;

    public UnityEvent<bool> OnPause;
    public event Action OnDifficultyIncreasing;

    /// <summary>
    /// Can only change by 1.
    /// </summary>
    public int TotalMonsterC
    {
        get
        {
            return _totalMonsterC;
        }
        set
        {
            if (Mathf.Abs(_totalMonsterC - value) != 1)
                throw new Exception("TotalMonsterC can only change by 1.");
            if (value < _totalMonsterC)
                _mDC++;
            _totalMonsterC = value;
            if (_totalMonsterC < 0)
                _totalMonsterC = 0;
        }
    }

    public void PauseByMenu(bool value)
    {
        if(_gameInProgress)
            OnPause.Invoke(value);
    }

    void Start()
    {
        Time.timeScale = 1f;
        Player.Instance.OnFreeze.AddListener(() => _aAC++);
        Player.Instance.OnFreeze.AddListener(() => 
        {
            _notification.text = "������������ ��������� �� 3 ���.";
            ShowNotification();
        });
        Player.Instance.OnTS.AddListener(() => _aAC++);
        _recordScores = int.MinValue;
        for (int i = 0; i < PlayerPrefs.GetInt("GameC"); i++)
        {
            string statistics = PlayerPrefs.GetString("Statistics_" + (i + 1));
            int scores = Statistics.Parse(statistics).Scores;
            if (_recordScores < scores)
                _recordScores = scores;
        }
        if (_recordScores < 0)
            _recordScores = 0;
        _recordNotification = () => 
        {
            if (Statistics.CalculateScores(_gT, _mDC, _aAC) > _recordScores)
            {
                _notification.text = "�� ����� ���� ������!";
                _recordNotification = null;
                ShowNotification();
            }
        };
        _gameInProgress = true;
        OnPause.AddListener((value) =>
        { 
            enabled = !value;
            Time.timeScale = value || !_gameInProgress ? 0f : 1f;
        });
        _records.DisplayAll(_recordsGroup, _recordsRect);
    }

    void FinishGame()
    {
        Time.timeScale = 0f;
        OnPause.Invoke(true);
        _gameInProgress = false;
        Statistics statistics = new Statistics(DateTime.Now, _mDC, _aAC, _gT);
        _records.AddStatistics(statistics);
        _gameOverPanel.gameObject.SetActive(true);
        _gameOverPanel.GetChild(2).gameObject.SetActive(_recordScores < statistics.Scores);
        Text[] statisticsTexts = new Text[]
        {
            _gameOverPanel.GetChild(3).GetComponent<Text>(),
            _gameOverPanel.GetChild(4).GetComponent<Text>(),
            _gameOverPanel.GetChild(5).GetComponent<Text>(),
            _gameOverPanel.GetChild(7).GetComponent<Text>()
        };
        statisticsTexts[0].text += " " + statistics.GameTimeAsString;
        statisticsTexts[1].text += " " + statistics._mDC;
        statisticsTexts[2].text += " " + statistics._aAC;
        statisticsTexts[3].text = statistics.Scores.ToString();
    }

    void Update()
    {
        _gT += Time.deltaTime;
        _timer.text = Statistics.GameTimeToString(_gT);
        _totalMonsterCText.text = TotalMonsterC.ToString();
        if (TotalMonsterC >= 10)
            FinishGame();
        _recordNotification?.Invoke();
        _difficultyStepTime -= Time.deltaTime;
        if(_difficultyStepTime <= 0f)
        {
            _difficultyStepTime = 20f;
            OnDifficultyIncreasing();
        }
    }

    async void ShowNotification()
    {
        _notification.gameObject.SetActive(true);
        await Task.Delay(3000);
        _notification.gameObject.SetActive(false);
    }

#if UNITY_EDITOR
    private void OnGUI()
    {
        if (GUILayout.Button("DeleteAll"))
        {
            PlayerPrefs.DeleteAll();
        }
        if (GUILayout.Button("Dead"))
        {
            TotalMonsterC = 10;
        }
    }
#endif
}