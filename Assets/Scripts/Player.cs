using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class Player : Singleton<Player>
{
    public CameraMoving _cameraMoving;
    [SerializeField] Button _freezeButton;
    [SerializeField] Button _tSButton;
    RocketConstructor _rocketConstructor;
    Vector3 _lastTouch;
    float _clickDelaying = -0.5f;
#if !UNITY_EDITOR && UNITY_ANDROID
    TouchPhase _expectedTouch;
#endif

    public UnityEvent OnFreeze;
    /// <summary>
    /// TS - TotalSuicide.
    /// </summary>
    public UnityEvent OnTS;

    new void Awake()
    {
        base.Awake();
        _cameraMoving = transform.parent.GetComponent<CameraMoving>();
        _rocketConstructor = new RocketConstructor();
        OnFreeze = _freezeButton.onClick;
        OnTS = _tSButton.onClick;
        OnFreeze.AddListener(() => HandleCooldown(_freezeButton, 5f));
        OnTS.AddListener(() => HandleCooldown(_tSButton, 10f));
        GameManager.Instance.OnPause.AddListener(value =>
        { 
            enabled = !value;
            SetControlActive(!value);
        });
    }

    void Update()
    {
        RaycastHit hit = GetDoubleClick();
        if (hit.transform)
            _rocketConstructor.StartRandomRocket(transform.position, 
                hit.point - transform.position);
    }

    public void SetControlActive(bool value)
    {
        _freezeButton.interactable = value;
        _tSButton.interactable = value;
        //_cameraMoving.enabled = value;
    }

    async void HandleCooldown(Button button, float cooldown)
    {
        button.interactable = false;
        Image img = button.transform.GetChild(0).GetComponent<Image>();
        Color origImgColor = img.color;
        img.color = new Color(origImgColor.r, origImgColor.g, origImgColor.b, origImgColor.a - 0.5f);
        Text cooldownText = button.transform.GetChild(1).GetComponent<Text>();
        cooldownText.gameObject.SetActive(true);
        while (cooldown >= 0f)
        {
            if (Time.timeScale > 0f)
                cooldown -= 0.1f;
            cooldownText.text = cooldown.ToString();
            await Task.Delay(100);
        }
        cooldownText.gameObject.SetActive(false);
        img.color = origImgColor;
        button.interactable = true;
    }

    /// <summary>
    /// Update method.
    /// </summary>
    /// <returns></returns>
    RaycastHit GetDoubleClick()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        Touch rightTouch = new Touch();
        bool isRightTouch = false;
        for (int i = 0; i < Input.touchCount && !isRightTouch; i++)
        {
            isRightTouch = Input.GetTouch(i).phase == _expectedTouch;
            rightTouch = Input.GetTouch(i);
        }
        if (isRightTouch && _clickDelaying < 0)
        {
            _lastTouch = rightTouch.position;
            _expectedTouch = TouchPhase.Began;
            _clickDelaying = 0.5f;
        }
        else if (isRightTouch && _clickDelaying >= 0)
        {
            RaycastHit resHit = default;
            Ray ray = _cameraMoving._camera.ScreenPointToRay(rightTouch.position);
            if (Physics.Raycast(ray, out RaycastHit hit, float.MaxValue))
                resHit = hit;
            _lastTouch = default;
            _expectedTouch = TouchPhase.Ended;
            return resHit;
        }
        if (_clickDelaying >= 0)
            _clickDelaying -= Time.deltaTime;
        return default;
#elif UNITY_EDITOR
        if (Input.GetMouseButtonDown(0) && _clickDelaying < 0)
        {
            _lastTouch = Input.mousePosition;
            _clickDelaying = 0.5f;
        }
        else if (Input.GetMouseButtonDown(0) && _clickDelaying >= 0)
        {
            RaycastHit resHit = default;
            Ray ray = _cameraMoving._camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit hit, float.MaxValue))
                resHit = hit;
            _lastTouch = default;
            return resHit;
        }
        if (_clickDelaying >= 0)
            _clickDelaying -= Time.deltaTime;
        return default;
#endif
    }
}