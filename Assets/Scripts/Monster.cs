using System.Threading.Tasks;
using UnityEngine;

public enum MonsterMotionState
{
    Idleness,
    Falling,
    Landing
}

public class Monster : MonoBehaviour
{
    public Rigidbody _rigidbody;
    MonsterMotionState _motionState = MonsterMotionState.Idleness;
    int _health = 3;
    Animator _animator;

    /// <summary>
    /// Player and other monsters can't interact with monster, if it equals true.
    /// </summary>
    public bool IsClosed { get; private set; } = true;

    public int Health
    {
        get
        {
            return _health;
        }
        set
        {
            if (!IsClosed)
            {
                _health = value;
                GetPlayerHit();
                if (_health <= 0)
                {
                    Die();
                    IsClosed = true;
                }
            }
        }
    }

    public void Idle()
    {
        _motionState = MonsterMotionState.Idleness;
        _animator.SetBool("Idles", true);
        _animator.SetBool("Evil", false);
    }

    public void RequestYelling()
    {
        if (Random.Range(0, 2) == 1)
        {
            _animator.SetTrigger("Yells");
            _animator.SetBool("Evil", true);
        }
    }

    void Start()
    {
        _animator = GetComponent<Animator>();
        _rigidbody = GetComponent<Rigidbody>();
        AnimationClip[] clips = _animator.runtimeAnimatorController.animationClips;
        for (int i = 0; i < clips.Length; i++)
        {
            if (clips[i].name == "Landing" || clips[i].name == "Idleness")
                clips[i].events[0].objectReferenceParameter = this;
        }
        Player.Instance.OnTS.AddListener(() => 
        { 
            IsClosed = false; 
            Health = 0;
        });
        GameManager.Instance.OnPause.AddListener(value => enabled = !value);
    }

    void Update()
    {
        CheckLanding();
        if (Vector3.Distance(transform.position, Player.Instance.transform.position) > 100f)
            Destroy(gameObject);
    }

    void CheckLanding()
    {
        bool landed = Physics.Raycast(transform.position, Vector3.down, 0.5f);
        if (landed && _motionState == MonsterMotionState.Falling)
        {
            IsClosed = false;
            _animator.SetTrigger("IsLanded");
            _animator.SetBool("IsFalling", false);
            _motionState = MonsterMotionState.Landing;
        }
        else if(!landed && _motionState == MonsterMotionState.Idleness)
        {
            _animator.SetBool("Idles", false);
            _animator.SetBool("IsFalling", true);
            _motionState = MonsterMotionState.Falling;
        }
    }

    void GetPlayerHit()
    {
        transform.rotation = Quaternion.LookRotation(Player.Instance.transform.position - transform.position);
        transform.localEulerAngles = new Vector3(0f, transform.localEulerAngles.y, 0f);
    }

    async void Die()
    {
        _animator.SetBool("IsDied", true);
        GameManager.Instance.TotalMonsterC--;
        await Task.Delay(8000);
        if(gameObject)
            Destroy(gameObject);
    }
}
