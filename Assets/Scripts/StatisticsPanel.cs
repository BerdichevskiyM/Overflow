using System;
using UnityEngine;
using UnityEngine.UI;

public class StatisticsPanel : IComparable<StatisticsPanel>
{
    GameObject _recordNotation;
    Statistics _statistics;

    public StatisticsPanel(Statistics statistics)
    {
        _statistics = statistics;
        Scores = _statistics.Scores;
    }

    public int Scores { get; private set; }

    public Transform Panel { get; private set; }

    public void Fill(Transform prefabPanel)
    {
        prefabPanel.GetChild(0).GetComponent<Text>().text = _statistics._date;
        string[] integerStatistics = new string[4] { _statistics.GameTimeAsString, _statistics._mDC.ToString(),
            _statistics._aAC.ToString(), _statistics.Scores.ToString() };
        for (int i = 1; i < 5; i++)
        {
            Text statisticsText = prefabPanel.transform.GetChild(i).GetComponent<Text>();
            if (i < 4 && integerStatistics[i - 1].Length > 8)
                statisticsText.text += integerStatistics[i - 1].Remove(8) + "...";
            else if (i == 4 && integerStatistics[i - 1].Length > 13)
                statisticsText.text += integerStatistics[i - 1].Remove(13) + "...";
            else
                statisticsText.text += integerStatistics[i - 1];
        }
        _recordNotation = prefabPanel.GetChild(5).gameObject;
        Panel = prefabPanel;
    }

    public void SetActiveRecordNote(bool value)
    {
        _recordNotation.SetActive(value);
    }

    public int CompareTo(StatisticsPanel obj)
    {
        return Math.Sign(Scores - obj.Scores);
    }
}
