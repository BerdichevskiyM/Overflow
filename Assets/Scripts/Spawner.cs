using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    /// <summary>
    /// SP - SpawnPlace.
    /// </summary>
    const float SPXZAbsBound = 0.4f;

    [SerializeField] bool _testMode = true;
    Monster[] _monsterPrefabs;
    SpawnPlace _spPrefab;
    [SerializeField] float _spawnTime = 2f;
    float _freezeTime = 3f;
    bool _freezed;
    float[] _spawnTimeRange = new float[] { 6f, 15f };
    int[] _healthMonsterRange = new int[] { 1, 5 };

    void Awake()
    {
        _monsterPrefabs = Resources.LoadAll<Monster>("Prefabs");
        _spPrefab = Resources.Load<SpawnPlace>("Prefabs/SpawnPlace");
    }

    void Start()
    {
        if (_testMode)
        {
            Spawn();
        }
        Player.Instance.OnFreeze.AddListener(() => _freezed = true);
        GameManager.Instance.OnDifficultyIncreasing += IncreaseDifficulty;
        GameManager.Instance.OnPause.AddListener(value => enabled = !value);
    }

    void Update()
    {
        if (!_testMode && !_freezed)
        {
            _spawnTime -= Time.deltaTime;
            if (_spawnTime <= 0f)
            {
                Spawn();
                _spawnTime = Random.Range(_spawnTimeRange[0], _spawnTimeRange[1]);
            }
        }
        else if (_freezed)
        {
            _freezeTime -= Time.deltaTime;
            if (_freezeTime <= 0f)
            {
                _freezeTime = 3f;
                _spawnTime = 2f;
                _freezed = false;
            }
        }

    }

    void IncreaseDifficulty()
    {
        _spawnTimeRange[0] = Mathf.Clamp(_spawnTimeRange[0] - 0.5f, 0.1f, 10f);
        _spawnTimeRange[1] = Mathf.Clamp(_spawnTimeRange[1] - 0.5f, 0.2f, 20f);
        _healthMonsterRange[0] = Mathf.Clamp(_healthMonsterRange[0] + 1, 0, 100);
        _healthMonsterRange[1] = Mathf.Clamp(_healthMonsterRange[1] + 1, 0, 100);
    }

    void Spawn()
    {
        Quaternion rotation = Quaternion.Euler(0f, Random.Range(0f, 360f), 0f);
        Monster monster = Instantiate(_monsterPrefabs.GetRandomItem(), new Vector3(), rotation);
        monster.transform.SetParent(transform);
        monster.transform.localPosition = new Vector3(Random.Range(-SPXZAbsBound, SPXZAbsBound),
            0f, Random.Range(-SPXZAbsBound, SPXZAbsBound));
        SpawnPlace sP = Instantiate(_spPrefab, monster.transform.position, rotation, transform);
        Vector3 spLocalPos = sP.transform.localPosition;
        sP.transform.localPosition = new Vector3(spLocalPos.x, SpawnPlace.YPos, spLocalPos.z);
        sP._monster = monster;
        monster.transform.SetParent(null);
        monster.Health = Random.Range(_healthMonsterRange[0], _healthMonsterRange[1]);
    }
}