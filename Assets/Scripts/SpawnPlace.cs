using UnityEngine;

public class SpawnPlace : MonoBehaviour
{
    /// <summary>
    /// Instance position y axis.
    /// </summary>
    public const float YPos = -5.34f;
    const float StartSideSize = 0.2f;
    const float MinMonsterYDistance = 3f;

    public Monster _monster;
    BoxCollider _collider;

    void Start()
    {
        _collider = GetComponent<BoxCollider>();
        _collider.size = new Vector3(StartSideSize, StartSideSize, StartSideSize);
    }

    void Update()
    {
        if (_monster)
        {
            Expand();
            if (_monster.transform.position.y - transform.position.y <= MinMonsterYDistance)
            {
                Destroy(gameObject);
                GameManager.Instance.TotalMonsterC++;
            }
        }
    }

    void Expand()
    {
        if (_collider.size.x < 1.2f)
            _collider.size += new Vector3(0.05f, 0.05f, 0.05f);
    }
}
