using UnityEngine;

public class CameraMoving : MonoBehaviour
{
    public Camera _camera;
    float _xRotation = 0f;
    [SerializeField] float _rotationSpeed = 0.15f;
    [SerializeField] float _zoomSpeed = 0.0001f;
    Vector2 _rotateDeceleration = new Vector2();
    [SerializeField] float _rotateResistance = 0.05f;
#if UNITY_EDITOR
    Vector3 _oldMousePosition;
#endif

    private void Start()
    {
        _camera = transform.GetChild(0).GetComponent<Camera>();
    }

    void Update()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        if (Input.touchCount == 1)
            MakeDeceleration();
        else if (Input.touchCount > 1)
            Zoom();
#elif UNITY_EDITOR
        if (Input.GetMouseButton(1))
            MakeDeceleration();
        else if (Mathf.Abs(Input.mouseScrollDelta.y) > 0.01f)
            Zoom();
        else
            _oldMousePosition = Input.mousePosition;
#endif
        Rotate();
    }

    void MakeDeceleration()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        Touch touch = Input.GetTouch(0);
        _rotateDeceleration = touch.rawPosition - touch.position;
        _rotateDeceleration = new Vector2(Mathf.Clamp(_rotateDeceleration.x, -12f, 12f), Mathf.Clamp(_rotateDeceleration.y, -8f, 8f));
#elif UNITY_EDITOR
        _rotateDeceleration = _oldMousePosition - Input.mousePosition;
        _rotateDeceleration = new Vector2(Mathf.Clamp(_rotateDeceleration.x, -12f, 12f), 
            Mathf.Clamp(_rotateDeceleration.y, -8f, 8f));
#endif
    }

    void Rotate()
    {
        Vector2 deltaPos = _rotateDeceleration * _rotationSpeed;
        _xRotation += deltaPos.y;
        _xRotation = Mathf.Clamp(_xRotation, -8f, 45f);
        transform.localEulerAngles = new Vector3(_xRotation, transform.localEulerAngles.y - deltaPos.x);
        _rotateDeceleration = Vector2.Lerp(_rotateDeceleration, new Vector2(), _rotateResistance);
    }

    void Zoom()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        float rawDifference = Vector2.Distance(Input.GetTouch(0).rawPosition, Input.GetTouch(1).rawPosition);
        float currentDifference = Vector2.Distance(Input.GetTouch(0).position, Input.GetTouch(1).position);
        float increment = (rawDifference - currentDifference) * _zoomSpeed;
        transform.localScale += new Vector3(0, increment, increment);
        transform.localScale = new Vector3(transform.localScale.x, Mathf.Clamp(transform.localScale.y, 0.4f, 1f),
            Mathf.Clamp(transform.localScale.z, 0.4f, 1f));
#elif UNITY_EDITOR
        float increment = -Input.mouseScrollDelta.y * _zoomSpeed;
        transform.localScale += new Vector3(0, increment, increment);
        transform.localScale = new Vector3(transform.localScale.x, Mathf.Clamp(transform.localScale.y, 0.4f, 1f),
            Mathf.Clamp(transform.localScale.z, 0.4f, 1f));
#endif
    }
}