﻿using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
    public static T Instance { get; private set; }

    public void Awake()
    {
        Instance = this as T;
    }
}