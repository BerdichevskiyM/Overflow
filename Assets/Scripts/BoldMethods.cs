using System.Collections.Generic;
using Random = UnityEngine.Random;

public static class BoldMethods
{
    public static T GetRandomItem<T>(this IList<T> items) => items[Random.Range(0, items.Count)];
}
