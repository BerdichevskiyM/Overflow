using System;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "NewRecords", menuName = "ScriptableObjects/Records", order = 51)]
public class Records : ScriptableObject
{
    const float StartRectHeight = 1337f;

    VerticalLayoutGroup _recordsGroup;
    RectTransform _recordsRect;
    [SerializeField] Transform _statisticsPanelPrefab;
    StatisticsPanel[] _statisticsPanels;

    public void DisplayAll(VerticalLayoutGroup layoutGroup, RectTransform rectTransform)
    {
        _recordsGroup = layoutGroup;
        _recordsRect = rectTransform;
        int gameC = PlayerPrefs.GetInt("GameC");
        if(_statisticsPanels == null)
        {
            _statisticsPanels = new StatisticsPanel[gameC];
            for(int i = 0; i < gameC; i++)
            {
                _statisticsPanels[i] = new StatisticsPanel(Statistics
                    .Parse(PlayerPrefs.GetString("Statistics_" + (i + 1))));
            }
        }
        Array.Sort(_statisticsPanels);
        Array.Reverse(_statisticsPanels);
        _recordsRect.sizeDelta = new Vector2(_recordsRect.sizeDelta.x,
                StartRectHeight * gameC + _recordsGroup.spacing * (gameC - 1));
        for (int i = 0; i < gameC; i++)
        {
            _statisticsPanels[i].Fill(Instantiate(_statisticsPanelPrefab, _recordsRect));
            _statisticsPanels[i].SetActiveRecordNote(i == 0);
        }
    }

    public void AddStatistics(Statistics statistics)
    {
        int gameC = PlayerPrefs.GetInt("GameC");
        gameC++;
        PlayerPrefs.SetInt("GameC", gameC);
        PlayerPrefs.SetString("Statistics_" + gameC, statistics.ToString());
        PlayerPrefs.Save();
        StatisticsPanel statisticsPanel = new StatisticsPanel(statistics);
        Array.Resize(ref _statisticsPanels, gameC);
        _statisticsPanels[gameC - 1] = statisticsPanel;
        Array.Sort(_statisticsPanels);
        Array.Reverse(_statisticsPanels);
        Display(statisticsPanel);
    }

    void Display(StatisticsPanel panel)
    {
        panel.Fill(Instantiate(_statisticsPanelPrefab, _recordsRect));
        int gameC = _statisticsPanels.Length;
        _recordsRect.sizeDelta = new Vector2(_recordsRect.sizeDelta.x,
                StartRectHeight * gameC + _recordsGroup.spacing * (gameC - 1));
        for (int i = 0; i < gameC; i++)
            _statisticsPanels[i].Panel.SetParent(_recordsRect.parent);
        for (int i = 0; i < gameC; i++)
        {
            _statisticsPanels[i].Panel.SetParent(_recordsRect);
            _statisticsPanels[i].SetActiveRecordNote(i == 0);
        }
    }
}
