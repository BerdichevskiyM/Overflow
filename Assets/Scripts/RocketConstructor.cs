using System.Threading.Tasks;
using UnityEngine;
using Obj = UnityEngine.Object;

public class RocketConstructor
{
    GameObject _rocketPrefab;
    Mesh[] _rocketMeshes;

    public RocketConstructor()
    {
        _rocketPrefab = Resources.Load<GameObject>("Prefabs/Rocket");
        _rocketMeshes = Resources.LoadAll<Mesh>("Meshes");
    }

    public Rocket StartRandomRocket(Vector3 position, Vector3 direction)
    {
        Rocket rocket = Obj.Instantiate(_rocketPrefab, position, Quaternion.identity)
            .AddComponent<Rocket>();
        Mesh rocketMesh = new Mesh();
        Mesh origMesh = _rocketMeshes.GetRandomItem();
        rocketMesh.vertices = origMesh.vertices;
        rocketMesh.triangles = origMesh.triangles;
        rocketMesh.bounds = origMesh.bounds;
        rocketMesh.uv = origMesh.uv;
        rocketMesh.uv2 = origMesh.uv2;
        rocketMesh.name = origMesh.name + "(Clone)";
        rocket.GetComponent<MeshFilter>().mesh = rocketMesh;
        if (origMesh.name == "Rocket08")
            rocket.transform.GetChild(0).position = new Vector3(0f, -1.76f, 0f);
        else if(origMesh.name == "Rocket29")
        {
            ParticleSystem flare = rocket.transform.GetChild(0).GetComponent<ParticleSystem>();
            flare.transform.position = new Vector3(0f, -3.79f, 0f);
            var lifeTime = flare.main.startLifetime;
            lifeTime.constantMax = 1.5f;
            lifeTime.constantMin = 0.3f;
            var size = flare.main.startSize;
            size.constant = 2f;
            flare.transform.localScale = new Vector3(2f, 2f, 2f);
        }
        rocket.Activate(direction);
        return rocket;
    }

    public class Rocket : MonoBehaviour
    {
        Vector3 _direction;
        bool _flies;
        Transform _explosion;

        public void Activate(Vector3 direction)
        {
            _direction = direction;
            _explosion = transform.GetChild(1);
            _flies = true;
            transform.rotation = Quaternion.FromToRotation(transform.up, direction);
        }

        void Update()
        {
            if(_flies)
            {
                transform.position = Vector3.MoveTowards(transform.position,
                    transform.position + _direction, 0.5f);
                transform.Rotate(Vector3.up, 5f);
            }
            if (Vector3.Distance(transform.position, Player.Instance.transform.position) > 100f)
                Destroy(gameObject);
        }

        async void StartDestroying()
        {
            await Task.Delay(1000);
            Destroy(_explosion.gameObject);
            Destroy(gameObject);
        }

        void OnCollisionEnter(Collision collision)
        {
            Monster collMonster = collision.transform.GetComponent<Monster>();
            _flies = false;
            _explosion.gameObject.SetActive(true);
            _explosion.SetParent(null);
            if (collMonster && !collMonster.IsClosed)
            {
                _explosion.position = collision.GetContact(0).point;
                collMonster._rigidbody.AddExplosionForce(40f, _explosion.position, 4f, 4f, ForceMode.Impulse);
                StartDestroying();
                collMonster.Health -= 1;
            }
            else 
            {
                _explosion.position = transform.position;
                StartDestroying();
            }
            gameObject.SetActive(false);
        }
    }
}