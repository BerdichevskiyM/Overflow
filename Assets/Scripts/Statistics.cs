using System;
using UnityEngine;

public struct Statistics
{
    public string _date;
    public int _mDC;
    public int _aAC;
    /// <summary>
    /// In seconds.
    /// </summary>
    public float _gT;

    /// <summary>
    /// "gameTime" is rounded to nearest integer seconds.
    /// </summary>
    /// <param name="date"></param>
    /// <param name="monsterDyingC"></param>
    /// <param name="abilityApplicationC"></param>
    /// <param name="gameTime">In seconds</param>
    public Statistics(DateTime date, int monsterDyingC, int abilityApplicationC, float gameTime)
    {
        _date = date.ToString();
        _mDC = monsterDyingC;
        _aAC = abilityApplicationC;
        _gT = Mathf.Round(gameTime);
    }

    public string GameTimeAsString => GameTimeToString(_gT);

    public static string GameTimeToString(float gameTime)
    {
        return TimeSpan.FromSeconds(gameTime).ToString("mm':'ss");
    }

    public int Scores => CalculateScores(_gT, _mDC, _aAC);

    public static int CalculateScores(float gameTime, int monsterDyingC, int abilityApplicationC)
    {
        return Mathf.RoundToInt(gameTime * monsterDyingC / (abilityApplicationC + 1));
    }

    public static Statistics Parse(string s)
    {
        string[] massive = s.Split(';');
        DateTime date = DateTime.Parse(massive[0]);
        int mDC = Convert.ToInt32(massive[1]);
        int aAC = Convert.ToInt32(massive[2]);
        float gT = Convert.ToInt32(massive[3]) * 60f + Convert.ToInt32(massive[4]);
        return new Statistics(date, mDC, aAC, gT);
    }

    public override string ToString()
    {
        return $"{_date};{_mDC};" +
            $"{_aAC};{TimeSpan.FromSeconds(_gT):mm';'ss}";
    }
}

public enum IntegerStatistics
{
    MonsterDying,
    AbilityApplication
}