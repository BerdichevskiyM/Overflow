using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public void StartNewGame() => SceneManager.LoadScene(0);

    public void Exit() => Application.Quit();
}