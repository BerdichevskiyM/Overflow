﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 T BoldMethods::GetRandomItem(System.Collections.Generic.IList`1<T>)
// 0x00000002 System.Void CameraMoving::Start()
extern void CameraMoving_Start_m9E2CC21AE7BFAFC3364C07AC2EA60322CF62DA1C (void);
// 0x00000003 System.Void CameraMoving::Update()
extern void CameraMoving_Update_mA0354D6C73F3616F4100D42BC37F948BA36961B3 (void);
// 0x00000004 System.Void CameraMoving::MakeDeceleration()
extern void CameraMoving_MakeDeceleration_mBE072C3E75A8F1B5BD8918ACD0DAD1F698918963 (void);
// 0x00000005 System.Void CameraMoving::Rotate()
extern void CameraMoving_Rotate_m9DA8C025368ACE36498E39573E6BEE3759C48DE3 (void);
// 0x00000006 System.Void CameraMoving::Zoom()
extern void CameraMoving_Zoom_m99892C762072356B9035014C10FAA071855EA31E (void);
// 0x00000007 System.Void CameraMoving::.ctor()
extern void CameraMoving__ctor_m5A92BB30AABD3C5CF7F9BB622DF73A7B1BEC78DC (void);
// 0x00000008 System.Void GameManager::add_OnDifficultyIncreasing(System.Action)
extern void GameManager_add_OnDifficultyIncreasing_m631F8C4C18AE3E9286028DA365B94750A8B53D5E (void);
// 0x00000009 System.Void GameManager::remove_OnDifficultyIncreasing(System.Action)
extern void GameManager_remove_OnDifficultyIncreasing_mF76D74030688A8F56DAF30FA8331F0A4C8D63A79 (void);
// 0x0000000A System.Int32 GameManager::get_TotalMonsterC()
extern void GameManager_get_TotalMonsterC_m6523EBFE7CA9B3F31EC78DEA340C3DA8D01E1DA7 (void);
// 0x0000000B System.Void GameManager::set_TotalMonsterC(System.Int32)
extern void GameManager_set_TotalMonsterC_mEA9185D79F18BF5907B27E39FB4945F02D5CAC09 (void);
// 0x0000000C System.Void GameManager::PauseByMenu(System.Boolean)
extern void GameManager_PauseByMenu_m7950A1EF5FE01AB0A57F07210AD46553C37DB8BD (void);
// 0x0000000D System.Void GameManager::Start()
extern void GameManager_Start_m26461AEF27E44DB8FECCBC19D6C9E228B658BF8E (void);
// 0x0000000E System.Void GameManager::FinishGame()
extern void GameManager_FinishGame_mAADD8ED18D134D398473A998BAC9F97937B5CF99 (void);
// 0x0000000F System.Void GameManager::Update()
extern void GameManager_Update_mC9303BA7C3117BD861F49F8E36151CC52117E6C1 (void);
// 0x00000010 System.Void GameManager::ShowNotification()
extern void GameManager_ShowNotification_mC0A12F4EB75490648D5ED5BCDA5C83F6FADC863A (void);
// 0x00000011 System.Void GameManager::.ctor()
extern void GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D (void);
// 0x00000012 System.Void GameManager::<Start>b__23_0()
extern void GameManager_U3CStartU3Eb__23_0_m26203DBC53838547FDC52AACF643520F38EBB603 (void);
// 0x00000013 System.Void GameManager::<Start>b__23_1()
extern void GameManager_U3CStartU3Eb__23_1_mF8D8AF73ED5485B0FDFC9797550414DB9382F900 (void);
// 0x00000014 System.Void GameManager::<Start>b__23_2()
extern void GameManager_U3CStartU3Eb__23_2_m523497E7CD8C572EAC9CE787BBCEC8AC6BF6FC21 (void);
// 0x00000015 System.Void GameManager::<Start>b__23_3()
extern void GameManager_U3CStartU3Eb__23_3_m911A425956B5ACB5E1213420293CDFBFCCAC6712 (void);
// 0x00000016 System.Void GameManager::<Start>b__23_4(System.Boolean)
extern void GameManager_U3CStartU3Eb__23_4_mE550EC23D343BD0B6FED64812435354E02B30BCB (void);
// 0x00000017 System.Void GameManager/<ShowNotification>d__26::MoveNext()
extern void U3CShowNotificationU3Ed__26_MoveNext_m146B08850E13AD67A5334B68AE3A2480F69CB488 (void);
// 0x00000018 System.Void GameManager/<ShowNotification>d__26::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CShowNotificationU3Ed__26_SetStateMachine_m9C2DAA538ACFCF983086B3769433B054DD9EEF33 (void);
// 0x00000019 System.Void Menu::StartNewGame()
extern void Menu_StartNewGame_m08A0B1F270075224B22BF6CFE3F0FCBDEAD5BBEF (void);
// 0x0000001A System.Void Menu::Exit()
extern void Menu_Exit_m2C8C35A855E5512CF373EB29E4251CA091642707 (void);
// 0x0000001B System.Void Menu::.ctor()
extern void Menu__ctor_m7EE9043A2E9DD126B6F69DD8AE3BC9CBDF6E2134 (void);
// 0x0000001C System.Boolean Monster::get_IsClosed()
extern void Monster_get_IsClosed_m15A637F4D5806E48C428C6CEE36FBC3D6F1777B3 (void);
// 0x0000001D System.Void Monster::set_IsClosed(System.Boolean)
extern void Monster_set_IsClosed_mC1E6F62E013BBA703F7861DD4C695D1A17740844 (void);
// 0x0000001E System.Int32 Monster::get_Health()
extern void Monster_get_Health_m8E39519CFC7D121DEC5DECD5245610E642845B59 (void);
// 0x0000001F System.Void Monster::set_Health(System.Int32)
extern void Monster_set_Health_m64DA7368E0EF547453A5890161BA1C311AE952A3 (void);
// 0x00000020 System.Void Monster::Idle()
extern void Monster_Idle_m7707C77E98BD0945E4DCC0BE1DDC6706BC97AD7D (void);
// 0x00000021 System.Void Monster::RequestYelling()
extern void Monster_RequestYelling_m5450231DD0E209E14BE89871EF615BCCC0428829 (void);
// 0x00000022 System.Void Monster::Start()
extern void Monster_Start_mA7A37BF82F6A37C0572D3A57BC3AF997E937EC4B (void);
// 0x00000023 System.Void Monster::Update()
extern void Monster_Update_mA746832FB24DD69DCA5698BEE10D1A37A71B706E (void);
// 0x00000024 System.Void Monster::CheckLanding()
extern void Monster_CheckLanding_m97E83160AC1BAA22F9CD9094D997DD8F351BD0A5 (void);
// 0x00000025 System.Void Monster::GetPlayerHit()
extern void Monster_GetPlayerHit_mC86FF6FA13D527342F5AB7EBF405E7FFF5DC3023 (void);
// 0x00000026 System.Void Monster::Die()
extern void Monster_Die_m8E097CA671BBBF16DF556CEAEDA555D249ACF2C4 (void);
// 0x00000027 System.Void Monster::.ctor()
extern void Monster__ctor_m7ED5C3D0C275D4C5980B4DD9B3CA82853E2E789B (void);
// 0x00000028 System.Void Monster::<Start>b__13_0()
extern void Monster_U3CStartU3Eb__13_0_mDA6ECBFF797672B5990895C4205D0A6AED6B4F85 (void);
// 0x00000029 System.Void Monster::<Start>b__13_1(System.Boolean)
extern void Monster_U3CStartU3Eb__13_1_mF2724866CFD1BE7B029F68F8D9C92B053E0F66DF (void);
// 0x0000002A System.Void Monster/<Die>d__17::MoveNext()
extern void U3CDieU3Ed__17_MoveNext_m0CC61DF76F5619D4338B7129498A8B77B2121A93 (void);
// 0x0000002B System.Void Monster/<Die>d__17::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CDieU3Ed__17_SetStateMachine_mC7761212CA09C35A4F605BE5C500845864450ACB (void);
// 0x0000002C System.Void Player::Awake()
extern void Player_Awake_m1131F11CF6BF6FBE6454601C7D9A94AC8F468A24 (void);
// 0x0000002D System.Void Player::Update()
extern void Player_Update_mBA04F1D6FE3C18037EA95DFAEEAA9977BFD49CD3 (void);
// 0x0000002E System.Void Player::SetControlActive(System.Boolean)
extern void Player_SetControlActive_m320B3082CD6C917DC7A7646EF9C8EE5722A7D171 (void);
// 0x0000002F System.Void Player::HandleCooldown(UnityEngine.UI.Button,System.Single)
extern void Player_HandleCooldown_mD026DF506142F1D996EABEBD12A39CC1507ACEDC (void);
// 0x00000030 UnityEngine.RaycastHit Player::GetDoubleClick()
extern void Player_GetDoubleClick_mBAB1BFDC5B54AC9BB2F64E5DA5C1F4C2E0B649BF (void);
// 0x00000031 System.Void Player::.ctor()
extern void Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7 (void);
// 0x00000032 System.Void Player::<Awake>b__9_0()
extern void Player_U3CAwakeU3Eb__9_0_m80615C79610D8D36033C5EBF9BC19E9FC98CDCAA (void);
// 0x00000033 System.Void Player::<Awake>b__9_1()
extern void Player_U3CAwakeU3Eb__9_1_m8E8DBE4A9A0BA98C8F94B4AEA21DC7FD1DCA07D2 (void);
// 0x00000034 System.Void Player::<Awake>b__9_2(System.Boolean)
extern void Player_U3CAwakeU3Eb__9_2_m4E9B1740788A7767E12EED530FEDA1BDAF020EC8 (void);
// 0x00000035 System.Void Player/<HandleCooldown>d__12::MoveNext()
extern void U3CHandleCooldownU3Ed__12_MoveNext_m143FF8439EE35FE42818DE2EE993592A26E9600E (void);
// 0x00000036 System.Void Player/<HandleCooldown>d__12::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CHandleCooldownU3Ed__12_SetStateMachine_mC38929C1F0EC8EC694BF2067745E25C10C6626BC (void);
// 0x00000037 System.Void Records::DisplayAll(UnityEngine.UI.VerticalLayoutGroup,UnityEngine.RectTransform)
extern void Records_DisplayAll_m92BB0BCEA71B7036B5845A40960E4C85E6FFBCAF (void);
// 0x00000038 System.Void Records::AddStatistics(Statistics)
extern void Records_AddStatistics_mB56821838D0285FFA5FE8D8DF05FD8C269DE7294 (void);
// 0x00000039 System.Void Records::Display(StatisticsPanel)
extern void Records_Display_m3F9964D66DD3C1797118BE0E7DD56C43A3A05A96 (void);
// 0x0000003A System.Void Records::.ctor()
extern void Records__ctor_mE0DEDD508A2FC1EE739F7DB194E65121D2911EA4 (void);
// 0x0000003B System.Void RocketConstructor::.ctor()
extern void RocketConstructor__ctor_mF85D5F8B0BB57CEF2BD9A9937A0A13B235C2E333 (void);
// 0x0000003C RocketConstructor/Rocket RocketConstructor::StartRandomRocket(UnityEngine.Vector3,UnityEngine.Vector3)
extern void RocketConstructor_StartRandomRocket_mE1996EC3D5C2EC198D266E0A6F7D2023E9AD3AC3 (void);
// 0x0000003D System.Void RocketConstructor/Rocket::Activate(UnityEngine.Vector3)
extern void Rocket_Activate_mCD9E982A1B9A01262FB6BCF418271430A5EA74DC (void);
// 0x0000003E System.Void RocketConstructor/Rocket::Update()
extern void Rocket_Update_m448B28DF8EEEBB97C60DC0DC3FD2F4B9BED46176 (void);
// 0x0000003F System.Void RocketConstructor/Rocket::StartDestroying()
extern void Rocket_StartDestroying_m9B39C7E922B89BFDA0DDC2527859BA5C09EF9D41 (void);
// 0x00000040 System.Void RocketConstructor/Rocket::OnCollisionEnter(UnityEngine.Collision)
extern void Rocket_OnCollisionEnter_m0045BAD738FBFDAC36EA65FC7C50AD4047AB6A1E (void);
// 0x00000041 System.Void RocketConstructor/Rocket::.ctor()
extern void Rocket__ctor_m9DB591F5E73EF925DADA203F03632CF41F1C72D9 (void);
// 0x00000042 System.Void RocketConstructor/Rocket/<StartDestroying>d__5::MoveNext()
extern void U3CStartDestroyingU3Ed__5_MoveNext_mB0B2FAAC9B324DB33D5AD021AB133421CB495F70 (void);
// 0x00000043 System.Void RocketConstructor/Rocket/<StartDestroying>d__5::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartDestroyingU3Ed__5_SetStateMachine_m25DD7D637C636CE5C8887907D2D3F6D1D8E4DBF3 (void);
// 0x00000044 T Singleton`1::get_Instance()
// 0x00000045 System.Void Singleton`1::set_Instance(T)
// 0x00000046 System.Void Singleton`1::Awake()
// 0x00000047 System.Void Singleton`1::.ctor()
// 0x00000048 System.Void SpawnPlace::Start()
extern void SpawnPlace_Start_m18705B851D68A0396E15B5071D2BCDBC965C2D64 (void);
// 0x00000049 System.Void SpawnPlace::Update()
extern void SpawnPlace_Update_m83A883C92F14C9C8228D7FF8FFA1BA344CADCABC (void);
// 0x0000004A System.Void SpawnPlace::Expand()
extern void SpawnPlace_Expand_m31B87E1EE6E93EE8F24F485B376718B733577A98 (void);
// 0x0000004B System.Void SpawnPlace::.ctor()
extern void SpawnPlace__ctor_m91F115E58948C78FEA9E9E62E8994549F580B796 (void);
// 0x0000004C System.Void Spawner::Awake()
extern void Spawner_Awake_m6F354EF9564D312409A7D72E3C614BD7624A6B62 (void);
// 0x0000004D System.Void Spawner::Start()
extern void Spawner_Start_m61D97BD980BD1B1877634A1E7626E47418D5D6D8 (void);
// 0x0000004E System.Void Spawner::Update()
extern void Spawner_Update_m8E44DB2210E6C1692B202D38D0867961E9720AA9 (void);
// 0x0000004F System.Void Spawner::IncreaseDifficulty()
extern void Spawner_IncreaseDifficulty_mA77CB1990B4EF6EC2BD7AC2569979B3AA6FF2AB9 (void);
// 0x00000050 System.Void Spawner::Spawn()
extern void Spawner_Spawn_m9A3438B8559369C0BC1578C2AE341CCA181E6B6C (void);
// 0x00000051 System.Void Spawner::.ctor()
extern void Spawner__ctor_m08E8D40AAA40F4329D8A95EEE2B2B6BE842CEB9C (void);
// 0x00000052 System.Void Spawner::<Start>b__10_0()
extern void Spawner_U3CStartU3Eb__10_0_m84E8A76579911F915457FA8FD4B3DA35532E5FD1 (void);
// 0x00000053 System.Void Spawner::<Start>b__10_1(System.Boolean)
extern void Spawner_U3CStartU3Eb__10_1_m1EF92C1E0AC7C1CB17C10C3BFB738E43485F454B (void);
// 0x00000054 System.Void Statistics::.ctor(System.DateTime,System.Int32,System.Int32,System.Single)
extern void Statistics__ctor_mE5EACFB92368DE256A456153804B902100F78EE4 (void);
// 0x00000055 System.String Statistics::get_GameTimeAsString()
extern void Statistics_get_GameTimeAsString_m988C1A2290D902A264D2964E609570850DB5595A (void);
// 0x00000056 System.String Statistics::GameTimeToString(System.Single)
extern void Statistics_GameTimeToString_m5CA0B76AA4FF7E2E10012CC64116FCB414F6015F (void);
// 0x00000057 System.Int32 Statistics::get_Scores()
extern void Statistics_get_Scores_m4A0D2D2B7DD109AF5D02DD70F8A9293444D3EE1F (void);
// 0x00000058 System.Int32 Statistics::CalculateScores(System.Single,System.Int32,System.Int32)
extern void Statistics_CalculateScores_mF0DB6F8048926551FD9D4FA2917D6EAAF8621239 (void);
// 0x00000059 Statistics Statistics::Parse(System.String)
extern void Statistics_Parse_m18230486ABC646277D786FD3D4D438C75AF99501 (void);
// 0x0000005A System.String Statistics::ToString()
extern void Statistics_ToString_m5B407B2AC07A44C0BEE71517DD076A8BDCF47E79 (void);
// 0x0000005B System.Void StatisticsPanel::.ctor(Statistics)
extern void StatisticsPanel__ctor_m3AD944C8289368E6AF508B825FC2FB73F012D947 (void);
// 0x0000005C System.Int32 StatisticsPanel::get_Scores()
extern void StatisticsPanel_get_Scores_m7875B2ACD75F9F95ACBCF4B3BB73F845C3F2B431 (void);
// 0x0000005D System.Void StatisticsPanel::set_Scores(System.Int32)
extern void StatisticsPanel_set_Scores_m618C553FB74449186E71367D15104AFB70405492 (void);
// 0x0000005E UnityEngine.Transform StatisticsPanel::get_Panel()
extern void StatisticsPanel_get_Panel_mCFEFB11401C45E166C4F9EC50C62D4B54C019521 (void);
// 0x0000005F System.Void StatisticsPanel::set_Panel(UnityEngine.Transform)
extern void StatisticsPanel_set_Panel_mACA96CEAF0AEB8C51A51733C45E35D922520F6C7 (void);
// 0x00000060 System.Void StatisticsPanel::Fill(UnityEngine.Transform)
extern void StatisticsPanel_Fill_mD9AAE2903EAE9E0CA9139C0A8EFFB8DEE6AF8D7D (void);
// 0x00000061 System.Void StatisticsPanel::SetActiveRecordNote(System.Boolean)
extern void StatisticsPanel_SetActiveRecordNote_m93F32EDAC690DD2D4C6B4DE21FD54AB625901A16 (void);
// 0x00000062 System.Int32 StatisticsPanel::CompareTo(StatisticsPanel)
extern void StatisticsPanel_CompareTo_m46756BD8BF9C8BA9B3BF98E0669D296C60B530A6 (void);
static Il2CppMethodPointer s_methodPointers[98] = 
{
	NULL,
	CameraMoving_Start_m9E2CC21AE7BFAFC3364C07AC2EA60322CF62DA1C,
	CameraMoving_Update_mA0354D6C73F3616F4100D42BC37F948BA36961B3,
	CameraMoving_MakeDeceleration_mBE072C3E75A8F1B5BD8918ACD0DAD1F698918963,
	CameraMoving_Rotate_m9DA8C025368ACE36498E39573E6BEE3759C48DE3,
	CameraMoving_Zoom_m99892C762072356B9035014C10FAA071855EA31E,
	CameraMoving__ctor_m5A92BB30AABD3C5CF7F9BB622DF73A7B1BEC78DC,
	GameManager_add_OnDifficultyIncreasing_m631F8C4C18AE3E9286028DA365B94750A8B53D5E,
	GameManager_remove_OnDifficultyIncreasing_mF76D74030688A8F56DAF30FA8331F0A4C8D63A79,
	GameManager_get_TotalMonsterC_m6523EBFE7CA9B3F31EC78DEA340C3DA8D01E1DA7,
	GameManager_set_TotalMonsterC_mEA9185D79F18BF5907B27E39FB4945F02D5CAC09,
	GameManager_PauseByMenu_m7950A1EF5FE01AB0A57F07210AD46553C37DB8BD,
	GameManager_Start_m26461AEF27E44DB8FECCBC19D6C9E228B658BF8E,
	GameManager_FinishGame_mAADD8ED18D134D398473A998BAC9F97937B5CF99,
	GameManager_Update_mC9303BA7C3117BD861F49F8E36151CC52117E6C1,
	GameManager_ShowNotification_mC0A12F4EB75490648D5ED5BCDA5C83F6FADC863A,
	GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D,
	GameManager_U3CStartU3Eb__23_0_m26203DBC53838547FDC52AACF643520F38EBB603,
	GameManager_U3CStartU3Eb__23_1_mF8D8AF73ED5485B0FDFC9797550414DB9382F900,
	GameManager_U3CStartU3Eb__23_2_m523497E7CD8C572EAC9CE787BBCEC8AC6BF6FC21,
	GameManager_U3CStartU3Eb__23_3_m911A425956B5ACB5E1213420293CDFBFCCAC6712,
	GameManager_U3CStartU3Eb__23_4_mE550EC23D343BD0B6FED64812435354E02B30BCB,
	U3CShowNotificationU3Ed__26_MoveNext_m146B08850E13AD67A5334B68AE3A2480F69CB488,
	U3CShowNotificationU3Ed__26_SetStateMachine_m9C2DAA538ACFCF983086B3769433B054DD9EEF33,
	Menu_StartNewGame_m08A0B1F270075224B22BF6CFE3F0FCBDEAD5BBEF,
	Menu_Exit_m2C8C35A855E5512CF373EB29E4251CA091642707,
	Menu__ctor_m7EE9043A2E9DD126B6F69DD8AE3BC9CBDF6E2134,
	Monster_get_IsClosed_m15A637F4D5806E48C428C6CEE36FBC3D6F1777B3,
	Monster_set_IsClosed_mC1E6F62E013BBA703F7861DD4C695D1A17740844,
	Monster_get_Health_m8E39519CFC7D121DEC5DECD5245610E642845B59,
	Monster_set_Health_m64DA7368E0EF547453A5890161BA1C311AE952A3,
	Monster_Idle_m7707C77E98BD0945E4DCC0BE1DDC6706BC97AD7D,
	Monster_RequestYelling_m5450231DD0E209E14BE89871EF615BCCC0428829,
	Monster_Start_mA7A37BF82F6A37C0572D3A57BC3AF997E937EC4B,
	Monster_Update_mA746832FB24DD69DCA5698BEE10D1A37A71B706E,
	Monster_CheckLanding_m97E83160AC1BAA22F9CD9094D997DD8F351BD0A5,
	Monster_GetPlayerHit_mC86FF6FA13D527342F5AB7EBF405E7FFF5DC3023,
	Monster_Die_m8E097CA671BBBF16DF556CEAEDA555D249ACF2C4,
	Monster__ctor_m7ED5C3D0C275D4C5980B4DD9B3CA82853E2E789B,
	Monster_U3CStartU3Eb__13_0_mDA6ECBFF797672B5990895C4205D0A6AED6B4F85,
	Monster_U3CStartU3Eb__13_1_mF2724866CFD1BE7B029F68F8D9C92B053E0F66DF,
	U3CDieU3Ed__17_MoveNext_m0CC61DF76F5619D4338B7129498A8B77B2121A93,
	U3CDieU3Ed__17_SetStateMachine_mC7761212CA09C35A4F605BE5C500845864450ACB,
	Player_Awake_m1131F11CF6BF6FBE6454601C7D9A94AC8F468A24,
	Player_Update_mBA04F1D6FE3C18037EA95DFAEEAA9977BFD49CD3,
	Player_SetControlActive_m320B3082CD6C917DC7A7646EF9C8EE5722A7D171,
	Player_HandleCooldown_mD026DF506142F1D996EABEBD12A39CC1507ACEDC,
	Player_GetDoubleClick_mBAB1BFDC5B54AC9BB2F64E5DA5C1F4C2E0B649BF,
	Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7,
	Player_U3CAwakeU3Eb__9_0_m80615C79610D8D36033C5EBF9BC19E9FC98CDCAA,
	Player_U3CAwakeU3Eb__9_1_m8E8DBE4A9A0BA98C8F94B4AEA21DC7FD1DCA07D2,
	Player_U3CAwakeU3Eb__9_2_m4E9B1740788A7767E12EED530FEDA1BDAF020EC8,
	U3CHandleCooldownU3Ed__12_MoveNext_m143FF8439EE35FE42818DE2EE993592A26E9600E,
	U3CHandleCooldownU3Ed__12_SetStateMachine_mC38929C1F0EC8EC694BF2067745E25C10C6626BC,
	Records_DisplayAll_m92BB0BCEA71B7036B5845A40960E4C85E6FFBCAF,
	Records_AddStatistics_mB56821838D0285FFA5FE8D8DF05FD8C269DE7294,
	Records_Display_m3F9964D66DD3C1797118BE0E7DD56C43A3A05A96,
	Records__ctor_mE0DEDD508A2FC1EE739F7DB194E65121D2911EA4,
	RocketConstructor__ctor_mF85D5F8B0BB57CEF2BD9A9937A0A13B235C2E333,
	RocketConstructor_StartRandomRocket_mE1996EC3D5C2EC198D266E0A6F7D2023E9AD3AC3,
	Rocket_Activate_mCD9E982A1B9A01262FB6BCF418271430A5EA74DC,
	Rocket_Update_m448B28DF8EEEBB97C60DC0DC3FD2F4B9BED46176,
	Rocket_StartDestroying_m9B39C7E922B89BFDA0DDC2527859BA5C09EF9D41,
	Rocket_OnCollisionEnter_m0045BAD738FBFDAC36EA65FC7C50AD4047AB6A1E,
	Rocket__ctor_m9DB591F5E73EF925DADA203F03632CF41F1C72D9,
	U3CStartDestroyingU3Ed__5_MoveNext_mB0B2FAAC9B324DB33D5AD021AB133421CB495F70,
	U3CStartDestroyingU3Ed__5_SetStateMachine_m25DD7D637C636CE5C8887907D2D3F6D1D8E4DBF3,
	NULL,
	NULL,
	NULL,
	NULL,
	SpawnPlace_Start_m18705B851D68A0396E15B5071D2BCDBC965C2D64,
	SpawnPlace_Update_m83A883C92F14C9C8228D7FF8FFA1BA344CADCABC,
	SpawnPlace_Expand_m31B87E1EE6E93EE8F24F485B376718B733577A98,
	SpawnPlace__ctor_m91F115E58948C78FEA9E9E62E8994549F580B796,
	Spawner_Awake_m6F354EF9564D312409A7D72E3C614BD7624A6B62,
	Spawner_Start_m61D97BD980BD1B1877634A1E7626E47418D5D6D8,
	Spawner_Update_m8E44DB2210E6C1692B202D38D0867961E9720AA9,
	Spawner_IncreaseDifficulty_mA77CB1990B4EF6EC2BD7AC2569979B3AA6FF2AB9,
	Spawner_Spawn_m9A3438B8559369C0BC1578C2AE341CCA181E6B6C,
	Spawner__ctor_m08E8D40AAA40F4329D8A95EEE2B2B6BE842CEB9C,
	Spawner_U3CStartU3Eb__10_0_m84E8A76579911F915457FA8FD4B3DA35532E5FD1,
	Spawner_U3CStartU3Eb__10_1_m1EF92C1E0AC7C1CB17C10C3BFB738E43485F454B,
	Statistics__ctor_mE5EACFB92368DE256A456153804B902100F78EE4,
	Statistics_get_GameTimeAsString_m988C1A2290D902A264D2964E609570850DB5595A,
	Statistics_GameTimeToString_m5CA0B76AA4FF7E2E10012CC64116FCB414F6015F,
	Statistics_get_Scores_m4A0D2D2B7DD109AF5D02DD70F8A9293444D3EE1F,
	Statistics_CalculateScores_mF0DB6F8048926551FD9D4FA2917D6EAAF8621239,
	Statistics_Parse_m18230486ABC646277D786FD3D4D438C75AF99501,
	Statistics_ToString_m5B407B2AC07A44C0BEE71517DD076A8BDCF47E79,
	StatisticsPanel__ctor_m3AD944C8289368E6AF508B825FC2FB73F012D947,
	StatisticsPanel_get_Scores_m7875B2ACD75F9F95ACBCF4B3BB73F845C3F2B431,
	StatisticsPanel_set_Scores_m618C553FB74449186E71367D15104AFB70405492,
	StatisticsPanel_get_Panel_mCFEFB11401C45E166C4F9EC50C62D4B54C019521,
	StatisticsPanel_set_Panel_mACA96CEAF0AEB8C51A51733C45E35D922520F6C7,
	StatisticsPanel_Fill_mD9AAE2903EAE9E0CA9139C0A8EFFB8DEE6AF8D7D,
	StatisticsPanel_SetActiveRecordNote_m93F32EDAC690DD2D4C6B4DE21FD54AB625901A16,
	StatisticsPanel_CompareTo_m46756BD8BF9C8BA9B3BF98E0669D296C60B530A6,
};
extern void U3CShowNotificationU3Ed__26_MoveNext_m146B08850E13AD67A5334B68AE3A2480F69CB488_AdjustorThunk (void);
extern void U3CShowNotificationU3Ed__26_SetStateMachine_m9C2DAA538ACFCF983086B3769433B054DD9EEF33_AdjustorThunk (void);
extern void U3CDieU3Ed__17_MoveNext_m0CC61DF76F5619D4338B7129498A8B77B2121A93_AdjustorThunk (void);
extern void U3CDieU3Ed__17_SetStateMachine_mC7761212CA09C35A4F605BE5C500845864450ACB_AdjustorThunk (void);
extern void U3CHandleCooldownU3Ed__12_MoveNext_m143FF8439EE35FE42818DE2EE993592A26E9600E_AdjustorThunk (void);
extern void U3CHandleCooldownU3Ed__12_SetStateMachine_mC38929C1F0EC8EC694BF2067745E25C10C6626BC_AdjustorThunk (void);
extern void U3CStartDestroyingU3Ed__5_MoveNext_mB0B2FAAC9B324DB33D5AD021AB133421CB495F70_AdjustorThunk (void);
extern void U3CStartDestroyingU3Ed__5_SetStateMachine_m25DD7D637C636CE5C8887907D2D3F6D1D8E4DBF3_AdjustorThunk (void);
extern void Statistics__ctor_mE5EACFB92368DE256A456153804B902100F78EE4_AdjustorThunk (void);
extern void Statistics_get_GameTimeAsString_m988C1A2290D902A264D2964E609570850DB5595A_AdjustorThunk (void);
extern void Statistics_get_Scores_m4A0D2D2B7DD109AF5D02DD70F8A9293444D3EE1F_AdjustorThunk (void);
extern void Statistics_ToString_m5B407B2AC07A44C0BEE71517DD076A8BDCF47E79_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[12] = 
{
	{ 0x06000017, U3CShowNotificationU3Ed__26_MoveNext_m146B08850E13AD67A5334B68AE3A2480F69CB488_AdjustorThunk },
	{ 0x06000018, U3CShowNotificationU3Ed__26_SetStateMachine_m9C2DAA538ACFCF983086B3769433B054DD9EEF33_AdjustorThunk },
	{ 0x0600002A, U3CDieU3Ed__17_MoveNext_m0CC61DF76F5619D4338B7129498A8B77B2121A93_AdjustorThunk },
	{ 0x0600002B, U3CDieU3Ed__17_SetStateMachine_mC7761212CA09C35A4F605BE5C500845864450ACB_AdjustorThunk },
	{ 0x06000035, U3CHandleCooldownU3Ed__12_MoveNext_m143FF8439EE35FE42818DE2EE993592A26E9600E_AdjustorThunk },
	{ 0x06000036, U3CHandleCooldownU3Ed__12_SetStateMachine_mC38929C1F0EC8EC694BF2067745E25C10C6626BC_AdjustorThunk },
	{ 0x06000042, U3CStartDestroyingU3Ed__5_MoveNext_mB0B2FAAC9B324DB33D5AD021AB133421CB495F70_AdjustorThunk },
	{ 0x06000043, U3CStartDestroyingU3Ed__5_SetStateMachine_m25DD7D637C636CE5C8887907D2D3F6D1D8E4DBF3_AdjustorThunk },
	{ 0x06000054, Statistics__ctor_mE5EACFB92368DE256A456153804B902100F78EE4_AdjustorThunk },
	{ 0x06000055, Statistics_get_GameTimeAsString_m988C1A2290D902A264D2964E609570850DB5595A_AdjustorThunk },
	{ 0x06000057, Statistics_get_Scores_m4A0D2D2B7DD109AF5D02DD70F8A9293444D3EE1F_AdjustorThunk },
	{ 0x0600005A, Statistics_ToString_m5B407B2AC07A44C0BEE71517DD076A8BDCF47E79_AdjustorThunk },
};
static const int32_t s_InvokerIndices[98] = 
{
	-1,
	1391,
	1391,
	1391,
	1391,
	1391,
	1391,
	1164,
	1164,
	1339,
	1154,
	1180,
	1391,
	1391,
	1391,
	1391,
	1391,
	1391,
	1391,
	1391,
	1391,
	1180,
	1391,
	1164,
	1391,
	1391,
	1391,
	1369,
	1180,
	1339,
	1154,
	1391,
	1391,
	1391,
	1391,
	1391,
	1391,
	1391,
	1391,
	1391,
	1180,
	1391,
	1164,
	1391,
	1391,
	1180,
	687,
	1360,
	1391,
	1391,
	1391,
	1180,
	1391,
	1164,
	685,
	1184,
	1164,
	1391,
	1391,
	498,
	1199,
	1391,
	1391,
	1164,
	1391,
	1391,
	1164,
	-1,
	-1,
	-1,
	-1,
	1391,
	1391,
	1391,
	1391,
	1391,
	1391,
	1391,
	1391,
	1391,
	1391,
	1391,
	1180,
	240,
	1351,
	2094,
	1339,
	1683,
	2126,
	1351,
	1184,
	1339,
	1154,
	1351,
	1164,
	1164,
	1180,
	841,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x02000010, { 2, 3 } },
	{ 0x06000001, { 0, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[5] = 
{
	{ (Il2CppRGCTXDataType)2, 770 },
	{ (Il2CppRGCTXDataType)2, 1010 },
	{ (Il2CppRGCTXDataType)2, 1359 },
	{ (Il2CppRGCTXDataType)2, 236 },
	{ (Il2CppRGCTXDataType)3, 4959 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	98,
	s_methodPointers,
	12,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	5,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
